#include <iostream>
#include <fstream>
#include <string>

void mudstLooper(const long nEventsToProcess=-1){

  gROOT->LoadMacro("loadMuDst.C");
  loadMuDst();
  
  StChain * chain = new StChain("StChain");

  StMuDstMaker *muDstMaker = new StMuDstMaker(0,0,"","fxtMuDst.list","",500);
  muDstMaker->SetStatus("*",0);
  muDstMaker->SetStatus("MuEvent",1);
  muDstMaker->SetStatus("PrimaryVertices",1);
  muDstMaker->SetStatus("PrimaryTracks",1);
  muDstMaker->SetStatus("GlobalTracks",1);
  muDstMaker->SetStatus("BTof*",1);

  chain->Init();

  //Statistics Counters
  Long64_t goodEvents(0);

  //Histograms
  TH1D *zVertexHisto     = new TH1D("zVertexHisto",";Z-Vertex (cm);",200,208,214);
  TH1D *goodTracksHisto  = new TH1D("goodTracksHisto",";N_{good tracks};",450,0,450);
  TH2D *massSquaredHisto = new TH2D("massSquaredHisto",";p (GeV/c); m^{2} (GeV^{2}/c^{4})",1000,-5,5,2500,-0.5,2.0);


  int status(0);
  long iEvent(0);
  while (status != 2){

    chain->Clear();
    status = chain->Make(iEvent);
    iEvent++;

    StMuDst *dst = muDstMaker->muDst();
    StMuPrimaryVertex *vertex = dst->primaryVertex();
    StMuEvent *event = dst->event();
    
    if (!dst || !vertex || !event)
      continue;

    //Cut on target Vertices
    if (vertex->position().z() < 210 || vertex->position().z() > 212)
      continue;
    
    //Cut out non-fxt triggers
    if (!event->triggerIdCollection().nominal().isTrigger(1))
      continue;

    //Increment the good event counter
    goodEvents++;

    //Fill Good Event Histograms
    zVertexHisto->Fill(vertex->position().z());

    //Track Stats
    double goodTracks(0);

    //Loop Over the Primary Tracks
    StMuTrack *track;
    for (Int_t iTrack=0; iTrack<dst->primaryTracks()->GetEntries(); iTrack++){

      track = dst->primaryTracks(iTrack);

      if (!track)
	continue;

      //Apply Track Cuts
      if (track->nHitsDedx() <= 0)
	continue;
      
      if ((double)track->nHitsFit(kTpcId) / (double)track->nHitsPoss(kTpcId) < 0.52)
	continue;

      //Increment the number of good tracks
      goodTracks++;

      //Don't use this track for TOF measurements if it doesn't have a match
      if (track->btofPidTraits().matchFlag() < 1)
	continue;

      //Don't use this track for TOF measurements if it has a non physica time of flight measurement
      if (track->btofPidTraits().timeOfFlight() <= 0.0)
	continue;
      
      //Compute the Mass Squared
      double totalP = track->p().mag();
      double tof    = track->btofPidTraits().timeOfFlight() * pow(10,-9);
      double length = track->btofPidTraits().pathLength();
      double charge = track->charge();

      double mass2 = pow(totalP,2) * ( pow((TMath::C()*100*tof)/length,2) - 1);
      
      //cout <<track->btofPidTraits().timeOfFlight() <<endl;
      //cout <<totalP <<" " <<mass2 <<"\n";
      massSquaredHisto->Fill(totalP*charge,mass2);
      

    }//End Loop Over Primary Tracks
    
    goodTracksHisto->Fill(goodTracks);

    //Stop if the user's number of events is requested
    if (nEventsToProcess > 0 && iEvent >= nEventsToProcess)
      break;
    
  }//End Loop Over Events

  TCanvas *canvas = new TCanvas("canvas","canvas",20,20,900,300);
  canvas->Divide(3,1);
  
  canvas->cd(1);
  zVertexHisto->Draw();

  canvas->cd(2);
  gPad->SetLogy();
  goodTracksHisto->Draw();

  canvas->cd(3);
  gPad->SetLogz();
  massSquaredHisto->Draw("COL");

  canvas->cd(4);
  

}
