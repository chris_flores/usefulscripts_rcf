#!/bin/bash

#Loop Over the Files in the Sub Directores and make a file list
# for input into the embedding maker

path=$1
listname=$2

for filename in $path/*;
do 
    for filename1 in $filename/*;
    do 
	echo $(pwd)/$filename1 >> $listname;
    done
done
