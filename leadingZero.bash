#!/bin/bash

#AUTHOR: Chris Flores | Dec. 9 2012

#This is a simple script to insert leading zeros into file names
#It is inteded for use only with files of the form *_[Numerals].*
#E.G. root files which come from the batch system at RCF

#FILENAME is the outputfile name
FILENAME=$1

if [ "${FILENAME}" = "" ]; then
    echo "ERROR: Please specify prefix for output files"
    exit 0
fi

#Print an example and ask the user if it is okay
echo "Really move for example:"
for a in *_[0-9].*; do
    b=$(echo $a | cut -f 4 -d '_')
    c=$FILENAME"000"$b
    echo "     "$a "to" $c
done

#Read the User's Answer
echo "     (yes/no)?"
read answer
if [ "${answer}" != "yes" ]; then
    echo "EXITING"
    exit 0
fi 

#If the use has answered 'yes' proceed with moving the files
for a in *_[0-9][0-9][0-9][0-9].*; do
    if [ "${a}" = "*_[0-9][0-9][0-9][0-9].*" ]; then
        echo "No files in the range 1000-9999"
        break
    fi
    b=$(echo $a | cut -f 4 -d '_')
    c=$FILENAME""$b
    mv $a $c
    echo "Moved " $a "to" $c
done

for a in *_[0-9][0-9][0-9].*; do
    if [ "${a}" = "*_[0-9][0-9][0-9].*" ]; then
        echo "No files in the range 100-999"
        break
    fi
    b=$(echo $a | cut -f 4 -d '_')
    c=$FILENAME"0"$b
    mv $a $c
    echo "Moved " $a "to" $c
done

for a in *_[0-9][0-9].*; do
    if [ "${a}" = "*_[0-9][0-9].*" ]; then
        echo "No files in the range 10-99"
        break
    fi
    b=$(echo $a | cut -f 4 -d '_')
    c=$FILENAME"00"$b
    mv $a $c
    echo "Moved " $a "to" $c
done

for a in *_[0-9].*; do
    if [ "${a}" = "*_[0-9].*" ]; then
	echo "No files in the range 0-9"
	break
    fi
    b=$(echo $a | cut -f 4 -d '_')
    c=$FILENAME"000"$b
    mv $a $c
    echo "Moved " $a "to" $c
done