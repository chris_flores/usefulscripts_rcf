#!/bin/bash

#AUTHOR: Chris Flores | Dec 9 2012

#A simple wrapper for hadd which will combine a directory of several root files to 
#a specified number of root files in a specified directory
#NOTE: THIS SCRIPT REQUIRES LEADING ZEROS IN FILESNAMES. SEE leadingZeros.bash

INPUTDIR=$1
OUTPUTDIR=$2
OUTPUTFILE=$3
NFILES=$4

#Check to make sure all all inputs are given
if [ "${INPUTDIR}" = "" ]; then
    echo "Please Specify Input Directory"
    exit 0
fi

if [ "${OUTPUTDIR}" = "" ]; then
    echo "Please Specify Output Directory"
    exit 0
fi

if [ "${NFILES}" = "" ]; then
    echo "Please Specify how many resultant root files are desired"
    exit 0
fi

#Change Directory to the Input Directory
cd $INPUTDIR
echo "Working in Directory: " 
pwd

#Make output directory if id doesn't already exist
if [ ! -d "$OUTPUTDIR" ]; then
    mkdir "$OUTPUTDIR"
    echo "Made new Directory: " $OUTPUTDIR
else
    echo $OUTPUTDIR " Already exists!"
    exit 0
fi 

#Find the number of .root files in the input directory
nInFiles=$(ls -l *.root | wc -l)
echo "There are a total of " $nInFiles " to be combined"

#Compute Number of original files will be combined into each resulting file
nCombine=$(($nInFiles/$NFILES))
nCombineRemainder=$(($nInFiles%$NFILES))
echo "Each output .root file will contain " $nCombine " files"
echo "Except the last one which will have: " $nCombine+$nCombineRemainder

#hadd the files
startFile=0000
endFile=0000
for i in `seq 1 $NFILES`; do
    echo "Combining set: "$i
    endFile=$(($startFile+$nCombine-1))
    if [ $i = $NFILES ]; then
	endFile=$(($endFile+$nCombineRemainder))
    fi
    fileList=""
    echo "StartFile: " $startFile "EndFile: " $endFile
    for ((n=$startFile; n<=$endFile; n++)); do
	k=
	printf -v k '%04d' $n
	newFile=$(ls *_$k.root)
	fileList="$fileList $newFile" 
    done
    #echo $fileList
    extension=".root"
    writeOut="$OUTPUTFILE$i$extension"
    echo $writeOut
    hadd $OUTPUTDIR/$writeOut $fileList
    startFile=$(($endFile+1))
done