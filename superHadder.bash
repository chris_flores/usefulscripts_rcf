#!/bin/bash

#AUTHOR: Chris Flores | Sept 2014

#Hadds all .root files together 

OUTFILE=$1


#Check to make sure inputs are given
if [ "${OUTFILE}" = "" ]; then
    echo "Why didn't you specify an output file? This isn't that hard!"
    exit 0
fi

#Get the List of Files to be added
declare -a fileList=(*.root)

for (( i=0; i < ${#fileList[*]}; i++ ))
do

    if [  $i -eq 0 ]; then
	hadd temp_$i.root ${fileList[$i]}
    fi


    if [ $i -ge 1 ]; then
	hadd temp_$i.root temp_$(($i-1)).root ${fileList[$i]}
	
	rm temp_$(($i-1)).root
    fi

done

mv temp_$((${#fileList[*]}-1)).root $OUTFILE




