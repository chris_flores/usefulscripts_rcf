#!/bin/bash

#This is aliases to getEvictedStats

#NOTE: This script assumes that a condor.log file that has "evicted"
#      in it did not finish.

#OUTPUT: This outputs two files. One listing the jobID and jobIndex 
#        of the evicted jobs and the other is the list of files 
#        that were assigned to those jobs

#The argument required is the directory in which the condor.log files exit
#NOTE: Make sure that all the condor.log files in this directory are from 
#      the same jobID

condorLogs=( $1/*condor.log )
jobID=$( basename ${condorLogs[0]} .condor.log | cut -d"_" -f1 | cut -d"d" -f2)

evictedRunFile=evictedRuns_$jobID.list
evictedRunFileList=evictedRunFiles_$jobID.list

#Print the evicted jobs to a file
grep -r "evicted" $1/*.condor.log | cut -d"." -f1 > $evictedRunFile

#Loop Over the entries in the file and cat their file lists together to
#make a single file containing all the failed files
while read file; 
do
    cat $file.list >> $evictedRunFileList

done < $evictedRunFile

numEvictedJobs=$( wc -l $evictedRunFile | cut -d" " -f1 )
numEvictedFiles=$( wc -l $evictedRunFileList | cut -d" " -f1 )

echo "Number of Jobs Evicted: $numEvictedJobs"
echo "Number of Files Evicted: $numEvictedFiles"